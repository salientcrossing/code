locals {
  ssh_to   = 22
  ssh_from = 22

  bsh_to   = 22
  bsh_from = 22

  http_to   = 80
  http_from = 80

  db_to   = 3306
  db_from = 3306

  https_to   = 443
  https_from = 443

}